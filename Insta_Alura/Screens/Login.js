import React, {Component} from 'react';
import {
    StyleSheet,
    View,
	Button,
	Text,
	Dimensions,
	AsyncStorage,
} from 'react-native';

import CustomInput from '../components/CustomInput';

export default class Login extends Component {

    constructor() {
        super();
        this.state = {
            user: '',
            password: '',
            message: '',
        }
    }

	logUser() {
		const uri = 'https://instalura-api.herokuapp.com/api/public/login';
		const requestInfo = {
			method: 'POST',
			body: JSON.stringify({
				login: this.state.user,
				senha: this.state.password
			}),
			headers: {
				'Content-type': 'application/json'
			}
		}

		fetch(uri, requestInfo)
			.then(response => {
				if(response.ok) {
					return response.text();
				}

				throw new Error("It wasn't possible to log you in! ;(");
			})
			.then(token => {
				AsyncStorage.setItem('token', token);
				AsyncStorage.setItem('user', this.state.user);

				console.warn(AsyncStorage.getItem('token'));
            })
            .catch(e => this.setState({message: e.message}))
	}

    render() {
        return (
			<View style={styles.container}>

				<View style={styles.form}>
					<Text style={styles.title}>Instalura</Text>

                    <CustomInput placeholder="User"
                        capitalize='none'
                        onChange={text => this.setState({user: text})}
                    />

                    <CustomInput placeholder="Password"
                        capitalize='none'
                        secure={true}
                        onChange={text => this.setState({password: text})}
                    />

					<Button title="Login" onPress={this.logUser.bind(this)}/>
				</View>

                <Text style={styles.message}>{this.state.message}</Text>
			</View>
        );
    }
}

const width = Dimensions.get('screen').width;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	form: {
		width: width * 0.8,
	},
	input: {
		height: 40,
		borderBottomWidth: 1,
		borderBottomColor: '#ddd',
	},
	title: {
		fontWeight: 'bold',
		fontSize: 26,
    },
    message: {
        marginTop: 15,
        color: '#e70c3c'
    }
});