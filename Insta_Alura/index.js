/**
 * @format
 */

import {Navigation} from 'react-native';
import Login from './Screens/Login';


import { Navigation } from "react-native-navigation";

Navigation.registerComponent(`navigation.playground.WelcomeScreen`, () => Login);

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      component: {
        name: "navigation.playground.WelcomeScreen"
      }
    }
  });
});