import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Text,
} from 'react-native';

export default class Likes extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    loadIcon(likeada) {
        return likeada ? require('../resources/images/s2-checked.png') : require('../resources/images/s2.png');
    }

    showLikes(likers) {
        // In JS true && expression will always evaluate expression
        return likers.length > 0 && <Text style={styles.like}>{likers.length} {likers.length > 1 ? 'curtidas' : 'curtida'}</Text>;
    }

    render() {
        const { photo, likeCallback } = this.props;

        return (
            <View>
                <TouchableOpacity onPress={() => {likeCallback(photo.id)}}>
                    <Image style={styles.likeButton}
                    source={this.loadIcon(photo.likeada)}/>
                </TouchableOpacity>

                {this.showLikes(photo.likers)}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    like: {
        fontWeight: 'bold',
        marginRight: 20,
    },
    likeButton: {
        height: 40,
        width: 40,
        marginBottom: 10,
    },
});
