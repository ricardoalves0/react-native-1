import React, {Component} from 'react';
import {
    StyleSheet,
    FlatList,
    Platform,
  } from 'react-native';
;

import Post from './Post';

export default class Feed extends Component {
    constructor() {
        super();
        this.state = {
            pictures: [],
        }
    }

    componentDidMount() {
        fetch('https://instalura-api.herokuapp.com/api/public/fotos/rafael')
            .then(response => response.json())
            .then(json => this.setState({pictures: json}))
            .catch(e => {
                console.warn('Something went wrong! ;(');
            });
    }

    findPhotoById(id) {
        return this.state.pictures.find(photo => photo.id === id);
    }

    updatePhoto(updatedPhoto) {
        const pictures = this.state.pictures.map(photo =>  photo.id === updatedPhoto.id ? updatedPhoto : photo);
        this.setState({pictures: pictures});
    }

    like(id) {
        const photo = this.findPhotoById(id);

        let newList = [];

        if (!photo.likeada) {
            newList = [
                ...photo.likers,
                {login: 'myUser'},
            ]
        } else {
            newList = newList.filter(element => {
                return element.login !== 'myUser';
            })
        }

        const updatedPhoto = {
            ...photo,
            likeada: !photo.likeada,
            likers: newList,
        }

        this.updatePhoto(updatedPhoto);
    }


    addComment(commentValue, inputComment, id) {
        if (commentValue === '') {
            return;
        }

        const photo = this.findPhotoById(id);

        const newList = [...photo.comentarios, {
            id: commentValue,
            login: 'myUser',
            texto: commentValue,
        }];

        const updatedPhoto = {
            ...photo,
            comentarios: newList,
        };

        this.updatePhoto(updatedPhoto);
        inputComment.clear();
    }

    render() {
        return (
            <FlatList style={styles.view}
                keyExtractor={(item, index) => index.toString()}
                data={this.state.pictures}
                renderItem={ ({item}) =>
                    <Post photo={item} likeCallback={this.like.bind(this)} commentCallback={this.addComment.bind(this)}/>
                }
            />
        )
    }
}

var margin = Platform.OS === 'ios' ? 20 : 0;

const styles = StyleSheet.create({
    view: {
        marginTop: margin,
    },
  });