import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
} from 'react-native';

import CustomInput from './CustomInput';

export default class CommentInput extends Component {
    constructor(props) {
        super(props)
        this.state = {
            commentValue: '',
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <CustomInput focus={true}
                    placeholder="Add a comment..."
                    ref={input => this.inputComment = input}
                    onChange={text => this.setState({commentValue: text})}
                    underlineColorAndroid="transparent"
                />

                <TouchableOpacity onPress={() => {
                    this.props.commentCallback(this.state.commentValue, this.inputComment, this.props.photo.id)
                    this.setState({commentValue: ""})
                }}>
                    <Image style={styles.sendIcon} source={require('../resources/images/send.png')}/>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    input: {
        height: 40,
        flex: 1,
    },
    sendIcon: {
        width: 30,
        height: 30,
    },
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
    }
});