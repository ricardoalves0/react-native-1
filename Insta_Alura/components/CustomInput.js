import React, { Component } from 'react';
import {
    TextInput,
    StyleSheet,
} from 'react-native';

export default class CustomInput extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TextInput placeholder={this.props.placeholder}
                style={styles.input}
                autoCapitalize={this.props.capitalize}
                secureTextEntry={this.props.secure}
                onChangeText={this.props.onChange}
                autoFocus={this.props.focus}
                ref={this.props.ref}
                underlineColorAndroid={this.props.underlineColorAndroid}
            />
        );
    }
}

const styles = StyleSheet.create({
    input: {
        height: 40,
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
    }
});
