import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    Dimensions,
} from 'react-native';

import CommentInput from './CommentInput';
import Likes from './Likes';

export default class Post extends Component {
    showSubtitle(photo) {
        if (photo.comentario === '') {
            return;
        }

        return (
            <View style={styles.comentario}>
                <Text style={styles.tituloComentario}>{photo.loginUsuario}</Text>
                <Text>{photo.comentario}</Text>
            </View>
        )
    }

    render() {
        const {commentCallback, likeCallback, photo} = this.props;

        return (
            <View>
                <View style={styles.profileInfo}>
                    <Image style={styles.profileImage} source={{uri: photo.urlPerfil}}/>
                    <Text>{photo.loginUsuario}</Text>
                </View>

                <Image style={styles.image} source={{uri: photo.urlFoto}}/>

                <View style={styles.footer}>
                    <Likes photo={photo} likeCallback={likeCallback}/>

                    {this.showSubtitle(photo)}

                    {photo.comentarios.map(comment =>
                        <View style={styles.comentario} key={comment.id}>
                            <Text style={styles.tituloComentario}>{comment.login}</Text>
                            <Text>{comment.texto}</Text>
                        </View>
                    )}
                </View>

                <CommentInput commentCallback={commentCallback} photo={photo}/>
            </View>
        );
    };
}

const width = Dimensions.get('screen').width;

const styles = StyleSheet.create({
    image: {
        width: width,
        height: width,
    },
    profileImage: {
        width: 40,
        height: 40,
        marginRight: 10,
        borderRadius: 20,
    },
    profileInfo: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    footer: {
        margin: 10,
    },
    comentario: {
        flexDirection: 'row',
    },
    tituloComentario: {
        fontWeight: 'bold',
        marginRight: 5,
    },
});